<?php
//
//// this code will help to prevent undefined index notice
//error_reporting(E_ALL & ~E_NOTICE);
////let's start the session
//session_start();
////store our posted values in the session variables
//$_SESSION['first_name'] = $_POST['first_name'];
//$_SESSION['last_name'] = $_POST['last_name'];
//$_SESSION['seip_id'] = $_POST['seip_id'];
//
//?><!--**/-->
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Course Form</title>
    <!--Bootstrap css-->
    <link rel="stylesheet" href="../assets/css/bootstrap.css">
    <!--Customized css-->
    <link rel="stylesheet" href="../assets/css/sheet.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <form class="form-horizontal" action="../students/store.php" method="post">
                <fieldset>
                    <legend>Course Form</legend>
                    <!--<div class="form-group">-->
                        <!--<label class="control-label" for="first_name">First Name:</label>-->
                        <!--<div>-->
                            <!--<input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">-->
                        <!--</div>-->
                    <!--</div>-->
                    <!--<div class="form-group">-->
                        <!--<label class="control-label" for="last_name">Last Name:</label>-->
                        <!--<div>-->
                            <!--<input type="text" class="form-control" id="last_name"  name="last_name" placeholder="Last Name">-->
                        <!--</div>-->
                    <!--</div>-->
                    <!--<div class="form-group">-->
                        <!--<label class="control-label" for="seip_id">SEIP ID:</label>-->
                        <!--<div>-->
                            <!--<input type="number" class="form-control" id="seip_id"  name="seip_id" placeholder="SEIP ID">-->
                        <!--</div>-->
                    <!--</div>-->

                    <div class="form-group">
                        <label class="control-label" for="course_title">Course Title:</label>
                        <div>
                            <input type="text" class="form-control" id="course_title"  name="course_title" placeholder="Course Title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="shift">Shift:</label>
                        <div>
                            <input type="text" class="form-control" id="shift"  name="shift" placeholder="Shift">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="batch_name">Batch Name:</label>
                        <div>
                            <input type="text" class="form-control" id="batch_name"  name="batch_name" placeholder="Batch Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="student_id">student_id:</label>
                        <div>
                            <input type="number" class="form-control" id="student_id"  name="student_id" placeholder="student_id">
                        </div>
                    </div>

                    <div class="form-group">
                        <div>
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../assets/js/jquery-1.12.4.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!--bootstrap js file-->
<script src="../assets/js/bootstrap.js"></script>
<!--Customized js file-->
<script src="../assets/js/app.js"></script>
</body>
</html>