
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Home</title>

    <!--Bootstrap css-->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!--Customized css-->
    <link rel="stylesheet" href="assets/css/sheet.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><i>MBC Foundation</i></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <form class="navbar-form navbar-left">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="index.php">Home <span class="sr-only">(current)</span></a></li>
                        <li><a href="students_table.php">Students Table</a></li>
<!--                        <li><a href="about.php">About US</a></li>-->

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Form <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="students/students_form.php">Students Form</a></li>
                                <li><a href="course/course_form.php">Course Form</a></li>
                                <!--                               <li><a href="customer_form.php">Customer Form</a></li>-->
                                <!--                               <li><a href="data_table.php">Data Table</a></li>-->
                                <li role="separator" class="divider"></li>
                                <!--                               <li><a href="logout.php">Log Out</a></li>-->
                            </ul>
                        </li>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>
</div>
<!--carousel part-->
<div class="container">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="Assets/images/slidea.jpg" alt="image" width="100%">
                <div class="carousel-caption">
                    <h3>Where does it come from?</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters,</p>
                </div>
            </div>
            <div class="item">
                <img src="Assets/images/slideb.jpg" alt="image" width="100%">
                <div class="carousel-caption">
                    <h3>Where can I get some?</h3>
                    <p>.It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>
            <div class="item">
                <img src="Assets/images/slidec.jpg" alt="image" width="100%">
                <div class="carousel-caption">
                    <h3>Where does it come from?</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters,</p>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>



<!--main part-->
<div class="container-fluid">
    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h1> Welcome heading</h1>
                    <h2> Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>
                    <hr>

                </div>
            </div>
            <div class="row" id="row_about">
                <div class="col-md-4" id="col_one">
                    <ul>
                        <li><h4> A little about me </h4></li>
                    </ul>
                    <p style="color:  #a5a9af;">		Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Veniam saepe aspernatur, id similique fuga ipsum nam. Dolorem dolor tenetur
                        dolores consequatur esse aut laudantium repudiandae, cum natus quia modi, sapiente.</p>
                    <hr>
                </div>
                <div class="col-md-4"  id="col_two">
                    <ul>
                        <li><h4>    I'm really good at..<h4></li>
                    </ul>
                    <p style="color:  #a5a9af;">		Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Veniam saepe aspernatur, id similique fuga ipsum nam. Dolorem dolor tenetur
                        dolores consequatur esse aut laudantium repudiandae, cum natus quia modi, sapiente.</p>
                    <hr>
                </div>
                <div class="col-md-4"  id="col_three">
                    <ul>
                        <li><h4>	Services I provide </h4></li>
                    </ul>
                    <p style="color:  #a5a9af;">		Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Veniam saepe aspernatur, id similique fuga ipsum nam. Dolorem dolor tenetur
                        dolores consequatur esse aut laudantium repudiandae, cum natus quia modi, sapiente.</p>
                    <hr>
                </div>
                <div class="col-md-4"  id="col_three">
                    <ul>
                        <li><h4>	Services I provide </h4></li>
                    </ul>
                    <p style="color:  #a5a9af;">		Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Veniam saepe aspernatur, id similique fuga ipsum nam. Dolorem dolor tenetur
                        dolores consequatur esse aut laudantium repudiandae, cum natus quia modi, sapiente.</p>
                    <hr>
                </div>
                <div class="col-md-4"  id="col_three">
                    <ul>
                        <li><h4>	Services I provide </h4></li>
                    </ul>
                    <p style="color:  #a5a9af;">		Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Veniam saepe aspernatur, id similique fuga ipsum nam. Dolorem dolor tenetur
                        dolores consequatur esse aut laudantium repudiandae, cum natus quia modi, sapiente.</p>
                    <hr>
                </div>
                <div class="col-md-4"  id="col_three">
                    <ul>
                        <li><h4>	Services I provide </h4></li>
                    </ul>
                    <p style="color:  #a5a9af;">		Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Veniam saepe aspernatur, id similique fuga ipsum nam. Dolorem dolor tenetur
                        dolores consequatur esse aut laudantium repudiandae, cum natus quia modi, sapiente.</p>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>
<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <h2 class="section-heading">Let's Get In Touch!</h2>
                <hr class="primary">
                <p>Ready to start your next project with us? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</p>
            </div>
            <div class="col-lg-4 col-lg-offset-2 text-center">
                <i class="fa fa-phone fa-3x sr-contact"></i>
                <p>123-456-6789</p>
            </div>
            <div class="col-lg-4 text-center">
                <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                <p><a href="mailto:your-email@your-domain.com">faisalahmed01944@gmail.com</a></p>
            </div>
        </div>
    </div>
</section>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="assets/js/bootstrap.min.js"></script>
</body>
</html>