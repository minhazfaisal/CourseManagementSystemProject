<?php

// connection to db
$db = new PDO('mysql:host=localhost;dbname=cms;charset=utf8mb4', 'root', '');

//build query
$query = "SELECT * FROM `students` WHERE id = ".$_GET['id'];

//execute the query using php
foreach ($db->query($query) as $row){
    $student = $row;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Show</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><i>MBC Foundation</i></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <form class="navbar-form navbar-left">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="../index.php">Home</a></li>
                        <li><a href="../students_table.php">Students Table</a></li>
                        <!--                        <li><a href="about.php">About US</a></li>-->

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Form <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="students_form.php">Students Form</a></li>
                                <li><a href="../course/course_form.php">Course Form</a></li>
                                <!--                               <li><a href="customer_form.php">Customer Form</a></li>-->
                                <!--                               <li><a href="data_table.php">Data Table</a></li>-->
                                <li role="separator" class="divider"></li>
                                <!--                               <li><a href="logout.php">Log Out</a></li>-->
                            </ul>
                        </li>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-">
                <table class="table table-inverse">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Full Name</th>
                    <th>Seip ID</th>
                    <th>Created At</th>
                    <th>Modified At</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?php echo $student['id'];?></td>
                    <td><?php echo $student['first_name']." ".$student['last_name'];?></td>
                    <td><?php echo $student['seip_id'];?></td>
                    <td><?php echo date("d/m/Y", strtotime($student['created_at']));?></td>
                    <td><?php echo date("d/m/Y", strtotime($student['modified_at']));?></td>
                </tr>
                </tbody>
                </table>
            <hr>
        </div>
        <br>

        <div class="col-md-1 col-md-offset-5">
            <button class="btn bg-info"><a href="students_form.php"> Add New </a></button></div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>