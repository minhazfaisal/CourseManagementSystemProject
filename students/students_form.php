<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Students Form</title>
    <!--Bootstrap css-->
    <link rel="stylesheet" href="../assets/css/bootstrap.css">
    <!--Customized css-->
    <link rel="stylesheet" href="../assets/css/sheet.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><i>MBC Foundation</i></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <form class="navbar-form navbar-left">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="../index.php">Home</a></li>
                        <li><a href="../students_table.php">Students Table</a></li>
                        <!--                        <li><a href="about.php">About US</a></li>-->

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Form <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="students_form.php">Students Form</a></li>
                                <li><a href="../course/course_form.php">Course Form</a></li>
                                <!--                               <li><a href="customer_form.php">Customer Form</a></li>-->
                                <!--                               <li><a href="data_table.php">Data Table</a></li>-->
                                <li role="separator" class="divider"></li>
                                <!--                               <li><a href="logout.php">Log Out</a></li>-->
                            </ul>
                        </li>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
<!--            <form class="form-horizontal" action="../course/course_form.php" method="post">-->
                <form class="form-horizontal" action="store.php" method="post">
                <fieldset>
                    <legend>Students Form</legend>
                    <div class="form-group">
                        <label class="control-label" for="first_name">First Name:</label>
                        <div>
                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="last_name">Last Name:</label>
                        <div>
                            <input type="text" class="form-control" id="last_name"  name="last_name" placeholder="Last Name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="seip_id">SEIP ID:</label>
                        <div>
                            <input type="number" class="form-control" id="seip_id"  name="seip_id" placeholder="SEIP ID" required>
                        </div>
                    </div>

                    <!--<div class="form-group">-->
                        <!--<label class="control-label" for="course_title">Course Title:</label>-->
                        <!--<div>-->
                            <!--<input type="text" class="form-control" id="course_title"  name="course_title" placeholder="Course Title">-->
                        <!--</div>-->
                    <!--</div>-->
                    <!--<div class="form-group">-->
                        <!--<label class="control-label" for="shift">Shift:</label>-->
                        <!--<div>-->
                            <!--<input type="text" class="form-control" id="shift"  name="shift" placeholder="Shift">-->
                        <!--</div>-->
                    <!--</div>-->
                    <!--<div class="form-group">-->
                        <!--<label class="control-label" for="batch_name">Batch Name:</label>-->
                        <!--<div>-->
                            <!--<input type="text" class="form-control" id="batch_name"  name="batch_name" placeholder="Batch Name">-->
                        <!--</div>-->
                    <!--</div>-->

                    <div class="form-group">
                        <div>
<!--                            <button type="submit" class="btn btn-default"><a id="next" href="../course/course_form.html" style="text-decoration: none">Next</a></button>-->

                             <button type="submit" class="btn btn-default">Next</button>
                        </div>
                    </div>
                    <?php //header("location: course_form.html"); ?>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../assets/js/jquery-1.12.4.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!--bootstrap js file-->
<script src="../assets/js/bootstrap.js"></script>
<!--Customized js file-->
<script src="../assets/js/app.js"></script>
</body>
</html>
