<?php

// connection to db
$db = new PDO('mysql:host=localhost;dbname=cms;charset=utf8mb4', 'root', '');

//build query
$query = "SELECT * FROM `students` WHERE id = ".$_GET['id'];

//execute the query using php
foreach ($db->query($query) as $row){
    $student = $row;
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Edit</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><i>MBC Foundation</i></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <form class="navbar-form navbar-left">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="../index.php">Home</a></li>
                        <li><a href="../students_table.php">Students Table</a></li>
                        <!--                        <li><a href="about.php">About US</a></li>-->

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Form <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="students_form.php">Students Form</a></li>
                                <li><a href="../course/course_form.php">Course Form</a></li>
                                <!--                               <li><a href="customer_form.php">Customer Form</a></li>-->
                                <!--                               <li><a href="data_table.php">Data Table</a></li>-->
                                <li role="separator" class="divider"></li>
                                <!--                               <li><a href="logout.php">Log Out</a></li>-->
                            </ul>
                        </li>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-sm-offset-4">

            <form action="update.php" method="post">
                <fieldset>
                    <legend>Student Infomation</legend>

                    <input type="hidden" class="form-control" id="id" name="id" placeholder="Enter ID"
                           value="<?=$student['id'];?>">

                    <div class="form-group">
                        <label for="first_name">First Name :</label>
                        <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter First Name"
                        value="<?=$student['first_name'];?>">
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name :</label>
                        <input value="<?=$student['last_name'];?>" type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter Last Name">
                    </div>
                    <div class="form-group">
                        <label for="seip_id">SEIP ID</label>
                        <input value="<?=$student['seip_id'];?>" type="text" class="form-control" id="seip_id" name="seip_id" placeholder="Enter Your SEIP ID">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </fieldset>
                <br>
                <hr>
                <div><button class="btn bg-info"><a href="students_form.php"> Add New </a></button></div>
            </form>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>